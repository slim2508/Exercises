def skolko_chisel(n):
    coun = str(n)
    return len(coun)

def dopolnenie(n):
    coun = list(n)
    newcoun = []
    i = 0
    while i < len(coun):
        if coun[i] == "0":
            newcoun.append("9")
        elif coun[i] == "1":
            newcoun.append("8")
        elif coun[i] == "2":
            newcoun.append("7")
        elif coun[i] == "3":
            newcoun.append("6")
        elif coun[i] == "4":
            newcoun.append("5")
        elif coun[i] == "5":
            newcoun.append("4")
        elif coun[i] == "6":
            newcoun.append("3")
        elif coun[i] == "7":
            newcoun.append("2")
        elif coun[i] == "8":
            newcoun.append("1")
        elif coun[i] == "9":
            newcoun.append("0")
        i = i + 1
    return newcoun

def obedinenie(n):
    result = ""
    for i in n:
        result += str(i)
    return result

def dlinna_spiskov(one, two):
    result = ""
    if len(one) == len(two) and one == two:
        result = "Равен"
    else: result = "Не равен"
    return result

def summa_s_isklucheniem():
    spis = [1,2,3,4,5]
    dele = [2,3]
    sum = 0
    result = []
    res = []
    for i in range(len(spis)):
        for j in range(len(dele)):
            if spis == dele:
                result.append(spis.remove(j))
    return spis

def treugolnik(a, b, c):
    result = ""
    if a + b > c or a + c > b or b + c > a:
        result += "Треугольник существует"
    else: result += "Треугольник не существует"
    return result

def posledovatelnost(a, b, c):
    result = ""
    if a+1 == b and b+1 == c:
        result += "являются последовательностью"
    elif b+1 == a and c+1 == a:
        result += "являются последовательностью"
    else:
        result += "не являются последовательностью"
    return result

def den_nedeli(num):
    result = ""
    if 1 <= num <= 7:
        if num == 1:
            result += "Понедельник"
        elif num == 2:
            result += "Вторник"
        elif num == 3:
            result += "Среда"
        elif num == 4:
            result += "Четверг"
        elif num == 5:
            result += "Пятница"
        elif num == 6:
            result += "Суббота"
        elif num == 7:
            result += "Воскресенье"
    else: result += "Введено некорректное значение. Введите от 1 до 7"
    return result

def deistvitelnie_chisla():
    result = ""
    try:
        a = eval(input("Введите число a "))
        b = eval(input("Введите число b "))
        result += "Введенное а больше" if a > b else "Введенное b больше"
    except NameError:
        result += "Введены не действительные числа"
    return result

def uravnenie(A, B):
    result = ""
    if A != 0:
        result += str((B-1)/(A-1))
    elif A == 0 and B == 1: result += "Решение любое число"
    elif A == 0 and B != 1: result += "Решений нет"
    return result
