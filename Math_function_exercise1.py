# Задание 5. Остаток от деления на 5.
def ostat(n):
    result = []
    i = 0
    while i < n + 1:
        if i % 5 == 3:
            result.append(i)
        i = i + 1
    return result


# Задание 4. Степень двойки
def stepen(n):
    result = []
    i = 0
    while i < n + 1:
        result.append(2 ** i)
        i = i + 1
    return result


# Задание 6. Деление на 3
def delit(n):
    result = ""
    if n % 3 == 0:
        result += "Делится"
    else:
        result += "Не делится"
    return result


# Задание 7. Факториал 3
def fact(n):
    fa = 1
    if n > 0:
        i = 0
        for i in range(1, n + 1):
            fa = fa * i
    else:
        print("Меньше 0")
    return fa


# Задание 8. Числа фибоначи
def fibonachi(n):
    one = 1
    result = [1]
    i = 1
    while one < n:
        if one > 1:
            result.append(result[i - 1] + result[i])
            i = i + 1
        else:
            result.append(one)
        one = one + 1
    return result


# Задание 9. Второе по величине числов функции.
def vtoroe_po_velichine(spis):
    spis = sorted(spis)
    del spis[len(spis) - 1]
    return spis


# Задание 10. Сумма нечетных чисел
def sum_nechet_chisel(n):
    summ = 0
    for i in range(1, n + 1):
        if i % 2 != 0:
            summ += i
    return summ


